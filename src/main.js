import { createApp } from 'vue'
import App from './App.vue'
import { createRouter, createWebHashHistory } from 'vue-router'
import '@fortawesome/fontawesome-free/css/all.css';

import HomeVue from './pages/HomeVue'
import LoginVue from './pages/LoginVue'
import PageError from './pages/PageError'
import ProductoPorId from './pages/ProductoPorId'
import CreateProduct from './pages/CreateProduct'
import NachoVue from './pages/NachoVue'
import AndresVue from './pages/AndresVue'


const routes = [

    {
        path:'/', component: HomeVue
    },
    {
        path:'/productoPorId/:numero', component: ProductoPorId, name: 'params'
    },
    {
        path:'/login', component: LoginVue
    },
    {
        path:'/create', component: CreateProduct
    },
    {
        path: '/:pathMatch(.*)*', component: PageError
    },
    {
        path:'/navbar', component: NachoVue
    },
    {
        path:'/AndresVue', component: AndresVue
    }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});


const app = createApp(App).use(router);

app.use(router);

app.mount("#app");

